﻿using UnityEngine;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using System.Collections.Generic;

public class DialogManager : MonoBehaviour {

    public TextAsset definitionsFile;
    public List<DialogSet> dialogSets;

    public static DialogManager instance;

    void Awake () {
        instance = this;
        dialogSets = new List<DialogSet> ();
        if (definitionsFile != null) {
            ParseFile (definitionsFile.text);
        }
    }

	// Use this for initialization
	void Start () {

        foreach (DialogSet dialogSet in dialogSets) {
            Debug.Log ("I have dialogSet belonging to " + dialogSet.actorName);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void ParseFile (string _source) {
        XDocument doc = XDocument.Parse (_source);

        foreach (XElement node in doc.Root.Elements ()) {
            DialogSet dialogSet = DialogSet.Create(node.Attribute("actorName").Value);
            AddDialogSet (dialogSet);
            foreach (XElement dialogNode in node.Elements ()) {
                dialogSet.AddDialog (Dialog.Create (dialogNode.Attribute ("id").Value, dialogNode.Element ("text").Value));
            }
        }
    }

    void AddDialogSet (DialogSet _dialogSet) {
        dialogSets.Add (_dialogSet);
    }

    public DialogSet Search (string _actorName) {
        for (int i = 0; i < dialogSets.Count; i++) {
            if (dialogSets[i].actorName == _actorName) {
                return dialogSets[i];
            }
        }
        Debug.LogError ("There's no DialogSet for Actor: " + _actorName);
        return null;
    }
}
