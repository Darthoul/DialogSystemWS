﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Dialog {

    public readonly string id;
    public string text;

    public static Dialog Create (string _id, string _text) {
        Dialog dialog = new Dialog (_id, _text);
        return dialog;
    }

    private Dialog (string _id, string _text) {
        id = _id;
        text = _text;
    }
}

public class DialogSet {

    public readonly string actorName;
    public List<Dialog> dialogs;

    public static DialogSet Create (string _actorName) {
        
        DialogSet dialogSet = new DialogSet (_actorName);
        return dialogSet;
    }

    private DialogSet (string _actorName) {
        actorName = _actorName;
        dialogs = new List<Dialog> ();
    }

    public void AddDialog (Dialog _dialog) {
        dialogs.Add (_dialog);
    }

    public Dialog Search (string _id) {
        for (int i = 0; i < dialogs.Count; i++) {
            if (dialogs[i].id == _id) {
                return dialogs[i];
            }
        }
        Debug.LogError ("There's no Dialog with ID " + _id + " from Actor " + actorName);
        return null;
    }
}