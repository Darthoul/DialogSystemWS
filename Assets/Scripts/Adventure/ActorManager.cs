﻿using UnityEngine;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using System.Collections.Generic;

public class ActorManager : MonoBehaviour {

    public TextAsset definitionsFile;
    public List<Actor> actors;

    public static ActorManager instance;

    void Awake () {
        instance = this;
        actors = new List<Actor> ();
        if (definitionsFile != null) {
            ParseFile (definitionsFile.text);
        }
    }

    // Use this for initialization
    void Start () {

        foreach (Actor actor in actors) {
            Debug.Log ("I have actor " + actor.id + " with name " + actor.name);
        }
    }

    // Update is called once per frame
    void Update () {

    }

    void ParseFile (string _source) {
        XDocument doc = XDocument.Parse (_source);

        foreach (XElement node in doc.Root.Elements ()) {
            AddActor (Actor.Create (node.Attribute ("id").Value, node.Attribute ("name").Value, node.Element ("sprite").Value));
        }
    }

    void AddActor (Actor _actor) {
        actors.Add (_actor);
    }

    public Actor Search (string _id) {
        for (int i = 0; i < actors.Count; i++) {
            if (actors[i].id == _id) {
                return actors[i];
            }
        }
        Debug.LogError("There's no Actor with ID: " + _id);
        return null;
    }
}
