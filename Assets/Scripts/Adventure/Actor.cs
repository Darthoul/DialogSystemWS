﻿using System.Collections;

public class Actor {

    public readonly string id;
    public string name;
    public string spriteName;
    private DialogSet dialogSet;
    public DialogSet DialogSet { get { return dialogSet; } }

    public static Actor Create (string _id, string _name, string _spriteName) {
        Actor actor = new Actor (_id, _name);
        actor.spriteName = _spriteName;
        return actor;
    }

    private Actor (string _id, string _name) {
        id = _id;
        name = _name;
    }

    public Actor Clone () {
        Actor actor = Create (id, name, spriteName);
        actor.GrabSet ();
        return actor;
    }

    private void GrabSet () {
        dialogSet = DialogManager.instance.Search (name);
    } 
}
