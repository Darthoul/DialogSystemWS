﻿using UnityEngine;
using System.Collections;

public class ActorContainer : MonoBehaviour {

    public string initialID;
    public Actor actor;

    public Transform dialogNode;
    public Transform spriteObject;

    void Awake () {
        dialogNode = transform.FindChild ("DialogNode");
        spriteObject = transform.FindChild ("Sprite");
    }

	// Use this for initialization
	void Start () {
        actor = ActorManager.instance.Search (initialID).Clone ();
        spriteObject.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Sprites/" + actor.spriteName);
        Debug.Log ("I am " + actor.name + " and my ID is " + actor.id);
        Say ("dg01");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Say (string _dialogID) {
        dialogNode.GetComponent<TextMesh>().text = actor.DialogSet.Search(_dialogID).text;
    }

    public void Announce () {

    }
}
